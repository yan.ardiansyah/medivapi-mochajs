const page = require('../../page/Auth/register')
const expect = require('chai').expect

const testCase = {
    positive : {
        sukses : 'Email Valid'
    },
    negative : {
        sukses : 'Email Tidak Valid'
    }
}

//randomInt
var min = 0
var max = 9999999999
var random = Math.floor(Math.random() * (+max - +min)) + +min

var validEmail = [
    'gmail.com', 'yahoo.com', 'hotmail.com', 'mymindstores.com', 'gmail.co.id', 'yahoo.co.id'
]

var invalidEmail = [
    'bigpond.net.au', 'aim.com', 'live.ca', 'chello.nl', 'centurytel.net', 'mac.com', 'windstream.net', 'sympatico.ca', 'skynet.be', 'bluewin.ch', 'blueyonder.co.uk', 'optusnet.com.au', 'juno.com', 'club-internet.fr', 'zonnet.nl', 'live.com.au', 'hetnet.nl', '	frontiernet.net', 'arcor.de', 'ntlworld.com', 'tin.it', 'live.it', 'planet.nl', 'mail.com', 'gmx.net', 'voila.fr', '	tiscali.co.uk', 'me.com', 'telenet.be', 'qq.com', 'home.nl', 'virgilio.it', 'aliceadsl.fr', 't-online.de', 'freenet.de', 'optonline.net', 'earthlink.net', 'sky.com', 'yahoo.co.jp', 'shaw.ca', 'tiscali.it', 'hotmail.de', 'rambler.ru', 'yahoo.com.au', 'yahoo.ca', 'charter.net', 'hotmail.es', 'yahoo.in', 'bellsouth.net', 'facebook.com', 'laposte.net', 'att.net', 'alice.it', 'yahoo.de', 'neuf.fr', 'yahoo.it', 'terra.com.br', 'bigpond.com', 'live.nl', 'ig.com.br', 'yahoo.es', 'live.co.uk', 'verizon.net', 'live.fr', 'sfr.fr', 'sbcglobal.net', 'hotmail.it', 'cox.net', 'mail.ru', 'bol.com.br', 'uol.com.br', 'libero.it', 'yandex.ru', 'web.de', 'gmx.de', 'free.fr', 'rediffmail.com', 'yahoo.co.in', 'yahoo.com.br', 'yahoo.co.uk', 'comcast.net', 'orange.fr', 'wanadoo.fr', 'yahoo.fr', 'hotmail.fr', 'hotmail.co.uk', 'yobmail.com', 'yopmail.com', 'ratcher5648.ga', '495-5454-695.ru','googlemail.com', 'net3mail.com', 'armyspy.com', 'cuvox.de', 'dayrep.com', 'einrot.com', 'fleckens.hu', 'gustr.com', 'jourrapide.com', 'rhyta.com', 'superrito.com', 'teleworm.us', 'besttempmail.com', 'meantinc.com', 'classesmail.com', 'powerencry.com', 'groupbuff.com', 'figurescoin.com', 'navientlogin.net', 'programmingant.com', 'castlebranchlogin.com', 'bestsoundeffects.com', 'vradportal.com', 'temporary-mail.net', 'matra.site', 'owlymail.com', 'iiron.us', 'privateme.site', 'deliverfreak.com', 'nomail.top', 'hasslemail.top', 'emailguy.info', 'bossemail.info', 'willymailly.com', 'mail2mail.site', 'enayu.com'
]

//randomChar
var result           = ''
var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
var charactersLength = characters.length

//looping berapa kali
for ( var i = 0; i < 10; i++ ) {
    //looping jumlah karakter yang tampil
    for( var k = 0; k < 5; k++){
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    invalidEmail.push(result + '.com')
    invalidEmail.push(result + '.net')
    invalidEmail.push(result + '.info')
    invalidEmail.push(result + '.me')
    result = ''
}


for(var i=0; i<invalidEmail.length; i++){
    var reg = {
        'name': 'NeverBounce', 
        'phone_number': '08' + random, 
        'email' : 'neverbounce@'+ invalidEmail[i], 
        'password' : 'secret',
        'c_password': 'secret',
        'registration_from': 'android',
        'referral_code': '',
        'pin': '000000',
        'gender': 'm',
        'dob': '1995-05-10',
        'is_tnc': '1'
    }
}

describe('Register' + testCase.negative, function(){
    for(var k=0; k<invalidEmail.length; k++){
        it(testCase.negative.sukses + ' ' + invalidEmail[k], function(done){
            page.register()
            .send(reg)
            .expect(200)
            .end(function(err, res){
                var pesan = "Email anda tidak vaid"
                done(err)
                console.log(res.body.status)
                expect(res.body.status.code).to.equal(500)
                expect(res.body.status.message).to.equal(pesan)
                console.log(res.body.data)
                expect(res.body.data.response.result).to.equal('invalid')
            })
        })
    }
})

describe('Register' + testCase.positive, function(){
    for(var k=0; k<validEmail.length; k++){
        it(testCase.positive.sukses + ' ' + validEmail[k], function(done){
            page.register()
            .send(reg)
            .expect(200)
            .end(function(err, res){
                var pesan = "Email anda tidak vaid"
                done(err)
                console.log(res.body.status)
                expect(res.body.status.code).to.equal(500)
                expect(res.body.status.message).to.equal(pesan)
                console.log(res.body.data)
                expect(res.body.data.response.result).to.equal('invalid')
            })
        })
    }
})