const expect = require('chai').expect
const page = require('../../page/Auth/login')

const testCase = {
    "positive": {
        "loginType1" : "User can login with valid account android",
        "loginType2" : "User can login with valid account dav",
        "loginType3" : "User can login with valid account cms"
    },
    "negative": {
        "loginType1" : "User can't login with invalid email android",
        "loginType11" : "User can't login with invalid password android",

        "loginType2" : "User can't login with valid account dav",
        "loginType3" : "User can't login with valid account cms",
        "error": "User mendapatkan error 404"
    }

}

describe('Login', function(){
    var email_android = 'kfmind4@gmail.com'
    var email_dav = ''
    var email_cms = ''
    var invalidEmail = 'kfmind90@gmail.com'
    var password_android = 'secret'
    var password_dav = ''
    var password_cms = ''
    var invalidPass = 'secretsssss'
    var type1 = 'android'
    var type2 = 'dav'
    var type3 = 'web'

    describe('Positive', function(){
        it(testCase.positive.loginType1, function(done){
            page.login(email_android, password_android, type1)
            .send(email_android, password_android, type1)
            .expect(200)
            .end(function(err, res){
                done(err)
                expect(res.body.status.code).to.equal(200)
                console.log(res.body.status.code)
    
                expect(res.body.data).to.have.property('token')
                expect(res.body.data).to.have.property('user')
                expect(res.body.data).to.have.property('address')
                expect(res.body.data).to.have.property('details')
    
                expect(res.body.data.user).to.have.property('id')
                expect(res.body.data.user).to.have.property('name')
                expect(res.body.data.user).to.have.property('code')
            })
        })
    })

    describe('Negative', function(){
        it(testCase.negative.loginType1, function(done){
            page.login(invalidEmail, invalidPass, type1)
            .send(invalidEmail, invalidPass, type1)
            .expect(501)
            .end(function(_err, res){
                done()
                var pesan = "Email tidak terdaftar."
                expect(res.body.status.code).to.equal(501)
                console.log(res.body.status.code)
                expect(res.body.status.message).to.have.string(pesan)
                console.log(res.body.status.message)
            })
        })

        it(testCase.negative.loginType11, function(done){
            page.login(email_android, invalidPass, type1)
            .send(email_android, invalidPass, type1)
            .expect(401)
            .end(function(_err, res){
                done()
                var pesan = "Username atau / dan password anda salah"
                expect(res.body.status.code).to.equal(401)
                console.log(res.body.status.code)
                expect(res.body.status.message).to.have.string(pesan)
                console.log(res.body.status.message)
            }) 
        })
    })

})