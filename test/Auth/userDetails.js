const page = require('../../page/Auth/userDetails')
const expect = require('chai').expect

var userToken = ''
var userCode = ''
var userName = ''
var userEmail = ''


const testCase = {
    'Positive': {
        'userCodeValid': 'User memiliki data valid'
    },
    'Negative': {
        'userCodeInvalid': 'User tidak ditemukan'
    }

}

var akun = {
    email : 'kfmind4@gmail.com', 
    password : 'secret', 
    type : 'android'
}

describe('User Details', function(){
    it('Login Berhasil', function(done){
        page.login(akun)
        .send(akun)
        .expect(200)
        .end(function(err, res){
            done(err)
            userToken = res.body.data.token
            console.log('usercode :' + res.body.data.user.code)
            console.log('name :' + res.body.data.user.name)
            console.log('email :' + res.body.data.user.email)
            userCode = res.body.data.user.code
            userName = res.body.data.user.name
            userEmail = res.body.data.user.email
        })
    })

    describe('Positive', function(){
        it(testCase.Positive.userCodeValid, function(done){
            page.userDetails()
            .set('Authorization', 'Bearer ' + userToken)
            .send()
            .expect(200)
            .end(function(err, res){
                done(err)
                if(res.status === 500){
                    console.log(res.body)
                }
                console.log('Mencocokkan User Code')
                expect(res.body.data.code).to.equal(userCode)
                expect(res.body.data.code).to.equal(userCode)
                expect(res.body.data.code).to.equal(userCode)

                console.log('Check Parameter di User Detail')
                expect(res.body.data).to.have.property('id' && 'name' && 'code' && 'phone_number' && 'email' && 'type' && 'medcash')
                expect(res.body.data.type).to.equal('android' || 'dav' || 'cms')
                expect(res.body.data.medcash.saldo).to.have.property('LAST_BALANCE' && 'WALLETID')
                expect(res.body.data.medcash).to.have.property('saldo' && 'point')
                expect(res.body.data.detail).to.have.property('id' && 'is_tnc' && 'is_topup' && 'pin_doku' && 'gender' && 'dob' && 'saldo_profit')
                expect(res.body.data.detail.progress_bar).to.have.property('tnc' && 'gender' && 'dob' && 'topup' && 'govid' && 'address')

                console.log('Check Unuseful parameter')
                expect(res.body.data).to.not.have.property('password' && 'created_at' && 'updated_at' && 'deleted_at')
                
                console.log('Check Address hanya menampilkan main address')
                for(var i=0; i<res.body.data.address.length; i++){
                    expect(res.body.data.address[i]).to.have.property('id' && 'main_address' != 0 && 'receiver_name' && 'province_id' && 'city_id' && 'district_id' && 'village_id' && 'code_pos' && 'address' && 'user_code' && 'phone_number' && 'city_name' && 'district_name' && 'village_name' && 'province_name')
                }
            })
        })
    })
})