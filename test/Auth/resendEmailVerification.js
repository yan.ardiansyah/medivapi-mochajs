const page = require('../../page/Auth/resendEmailVerification')
const expect = require('chai').expect

const testCase = {
    positive : {
        sukses : 'Email berhasil dikirimkan dan hanya dikirmkan 1x perhari',
        verified : 'Email sudah diverifikasi'
    },
    negative : {
        gagal : 'Error, email parameter dikosongkan'
    }
}

var emailVerified = 'kfmind4@gmail.com'
var emailUnverified = 'kfmind3@gmail.com'
var email = ''

describe('Resend Email Verification', function(){
    describe('Positive', function(){
        it(testCase.positive.sukses, function(done){
            page.resendEmail(emailUnverified)
            .send(emailUnverified)
            .expect(200)
            .end(function(err, res){
                done(err)
                if(res.status == 200){
                    switch(res.body.status.code){
                        case 200:
                                expect(res.body.status.code).to.equal(200)
                                expect(res.body.status.message).to.equal('Silahkan cek email anda untuk melakukan verifikasi.')
                                console.log(res.body.status)
                                console.log('Check Unuseful Parameter')
                                expect(res.body).to.not.have.property('data')
                                break
                        case 503:
                                expect(res.body.status.code).to.equal(503)
                                expect(res.body.status.message).to.equal('Batas pengiriman email anda sudah terpenuhi, silahkan hubungi Cs apabila email masih belum terkirim.')
                                console.log(res.body.status)
                                console.log('Check Unuseful Parameter')
                                expect(res.body).to.not.have.property('data')
                                break
                    }
                } else {
                    console.log(res.body.status)
                }
            })
        })

        it(testCase.positive.verified, function(done){
            page.resendEmail(emailVerified)
            .send(emailVerified)
            .expect(500)
            .end(function(err, res){
                done(err)
                expect(res.body.status.code).to.equal(500)
                console.log(res.body.status)
            })
        })
    })

    describe('Negative', function(){
        it(testCase.negative.gagal, function(done){
            page.resendEmail(email)
            .send(email)
            .expect(500)
            .end(function(err, res){
                done(err)
                console.log(res.status)
            })
        })
    })
})