const expect = require('chai').expect
const page = require('../../page/User/checkReferral')

const testCase = {
    positive: {
        sukses: 'Referral Valid',
        tanpa: 'Referral tidak diisi'
    },
    negative: {
        gagal: 'Referral Invalid'
    }
}

var akun = {
    email : 'kfmind4@gmail.com',
    password : 'secret',
    type : 'android'
}

var referral = ''
var invalidReferral = 'testuihad799'

describe('Login', function(){
    it('Sukses', function(done){
        page.login(akun)
        .send(akun)
        .expect(200)
        .end(function(err, res){
            done(err)
            console.log(res.body.data.details.referral_code)
            referral = res.body.data.details.referral_code
        })
    })
})

const checkRefAPI = page.supertest(process.env.DEV_API + '/api/user/referral/check?referral_code=' + referral)
const checkRef = () => checkRefAPI.get('')

const checkRefAPI1 = page.supertest(process.env.DEV_API + '/api/user/referral/check?referral_code=' + invalidReferral)
const checkRef1 = () => checkRefAPI1.get('')

const checkRefAPI2 = page.supertest(process.env.DEV_API + '/api/user/referral/check?referral_code=')
const checkRef2 = () => checkRefAPI2.get('')

describe('Check Referral', function(){
    it(testCase.positive.sukses, function(done){
        checkRef()
        .send()
        .expect(200)
        .end(function(err, res){
            done(err)
            console.log(res.body)
        })
    })

    it(testCase.negative.gagal, function(done){
        checkRef1()
        .send()
        .expect(500)
        .end(function(err, res){
            done(err)
            console.log(res.body)
        })
    })

    it(testCase.positive.tanpa, function(done){
        checkRef2()
        .send()
        .expect(200)
        .end(function(err, res){
            done(err)
            console.log(res.body)
        })
    })
})