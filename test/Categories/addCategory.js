const page = require('../../page/Categories/addCategory')
const expect = require('chai').expect
const fs = require('fs')

const testCase = {
    positive : {
        sukses : 'Kategori berhasil ditambahkan'
    },
    negative : {
        gagal : 'Kategori gagal ditambahkan'
    }
}

var name = 'Auto Kategori'
var file_image = fs.readFileSync('/medivapi/cute.png', 'utf-8')

var akun = {
    email : 'yan-kf@mail.com',
    password : 'secret',
    type : 'cms'
}

var userToken = ''

describe('Login', function(){
    it('Berhasil', function(done){
        page.login(akun)
        .send(akun)
        .expect(200)
        .end(function(err, res){
            done(err)
            console.log(res.body.data.token)
            userToken = res.body.data.token
        })
    })
})

describe('Add Category', function(){
    describe('Positive Test', function(){
        it(testCase.positive.sukses, function(){
            page.addCategory(name, file_image)
            .set('Authorization', 'Bearer ' + userToken)
            .field('name', 'Auto Kategori')
            .attach(file_image)
            .expect(200)
            .end(function(err, res){
                console.log(res.body)
            })
        })
    })
})