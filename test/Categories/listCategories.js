const page = require('../../page/Categories/listCategories')
const expect = require('chai').expect

const testCase = {
    positive : {
        sukses : 'List BErhasil'
    }
}

describe('List Categories', function(){
    describe('Positive', function(){
        it(testCase.positive.sukses, function(done){
            page.listCategories()
            .expect(200)
            .end(function(err, res){
                done(err)
                for(var i=0; i<res.body.data.length; i++){
                    expect(res.body.data[i]).to.have.property('id' && 'name' && 'totalProduct')
                    console.log(res.body.data[i].id)
                    console.log(res.body.data[i].name)
                    console.log(res.body.data[i].totalProduct)
                }
            })
        })
    })
})