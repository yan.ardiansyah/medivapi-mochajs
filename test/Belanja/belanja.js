const page = require('../../page/Belanja/belanja')
const expect = require('chai').expect

const testCase = {
    positive: {
        sukses : 'Belanja sukses'
    },
    negative: {
        gagal : 'Belanja gagal',
        metodesalah : 'Metode yang anda masukkan salah',
        pinsalah : 'PIN yang anda masukkan salah',
        ordercodesalah : 'Order Code anda salah',
        amountsalah : 'Amount anda Salah',
        tipesalah : 'Tipe yang anda salah'
    }
}

//login
var userToken = ''
var akun = {
    email : 'kfmind4@gmail.com',
    password : 'secret',
    type : 'android'
}

//homepage
var login = 'ok'

//checkout
var cartCode = ''
var productClientId = ''
var productClientId1 = ''
var addressId = ''
var servicejne = 'REG19'
var servicewahana = 'WAHANA-REG'

//payment
var metode = 'doku'
var metodesalah = 'dok'
var pin = '000000'
var pinsalah = '012345'
var ordercode = ''
var ordercodesalah = 'Ojadkfbakbdfasdf'
var amount = ''
var amountsalah = '987'
var tipe = 'T'

//Belanja GAGAL
describe(testCase.negative.gagal, function(){
    it('Login Sukses', function(done){
        page.login(akun)
        .send(akun)
        .expect(200)
        .end(function(err, res){
            done(err)
            console.log(res.body.data.token)
            userToken = res.body.data.token
            for(var i=0; i<res.body.data.address.length; i++){
                if(res.body.data.address[i].main_address == '1'){
                    console.log(res.body.data.address[i])
                    addressId = res.body.data.address[i].id
                }
            }
        })
    })

    it('Meload data produk di homepage', function(done){
        page.home(login)
        .set('Authorization', 'Bearer ' + userToken)
        .send(login)
        .expect(200)
        .end(function(err, res){
            done(err)
            if(res.body.data[1].see_all == 'product'){
                console.log(res.body.data[1].data[0].product_client_id)
                productClientId = res.body.data[1].data[0].product_client_id
                productClientId1 = res.body.data[1].data[1].product_client_id
            }
        })
    })
    
    it('Menambahkan produk ke cart', function(done){
        if(done){
            page.cartadd()
            .set('Authorization', 'Bearer ' + userToken)
            .send(
                {
                    'items':[
                        {
                            'product_client_id': productClientId,
                            'qty': "1",
                            'note': "Penerima Jennie Kim",
                            'type':"products"
                        }
                    ]
                }
            )
            .expect(200)
            .end(function(err, res){
                done(err)
                console.log(res.body.data)
            })
            } else {
                page.cartadd()
            .set('Authorization', 'Bearer ' + userToken)
            .send(
                {
                    'items':[
                        {
                            'product_client_id': productClientId1,
                            'qty': "1",
                            'note': "Penerima Jennie Kim",
                            'type':"products"
                        }
                    ]
                }
            )
            .expect(200)
            .end(function(err, res){
                done(err)
                console.log(res.body.data)
            })
            }
    })

    it('Data Cart Berhasil', function(done){
        page.cartlist()
        .set('Authorization', 'Bearer ' + userToken)
        .expect(200)
        .end(function(err, res){
            done(err)
            expect(userToken).to.equal(res.body.status.token)
            console.log(res.body.data.cart.code)
            cartCode = res.body.data.cart.code
            for(var i=0; i<res.body.data.cart_details.length; i++){
                console.log(res.body.data.cart_details[i].product_client_id)
                productClientId = res.body.data.cart_details[i].product_client_id
            }
        })
    })

    it('PreCheckout Berhasil', function(done){
        page.preCheckout(cartCode, productClientId)
        .set('Authorization', 'Bearer ' + userToken)
        .send(cartCode, productClientId)
        .expect(200)
        .end(function(err, res){
            done(err)
            console.log(res.body.data)
        })
    })

    it('Checkout Berhasil', function(done){
        page.checkout(addressId, servicejne)
        .set('Authorization', 'Bearer ' + userToken)
        .send(addressId, servicejne)
        .expect(200)
        .end(function(err, res){
            done(err)
            console.log(res.body.data)
            ordercode = res.body.data.order.code
            amount = res.body.data.order.total_amount_final
        })
    })

    it(testCase.negative.metodesalah, function(done){
        page.payment(metodesalah, pin, ordercode, amount, tipe)
        .set('Authorization', 'Bearer ' + userToken)
        .send(metodesalah, pin, ordercode, amount, tipe)
        .expect(200)
        .end(function(err, res){
            done(err)
            console.log(res.body.data)
        })
    })
    
    it(testCase.negative.pinsalah, function(done){
        page.payment(metode, pinsalah, ordercode, amount, tipe)
        .set('Authorization', 'Bearer ' + userToken)
        .send(metode, pinsalah, ordercode, amount, tipe)
        .expect(200)
        .end(function(err, res){
            done(err)
            console.log(res.body.data)
        })
    })

    it(testCase.negative.ordercodesalah, function(done){
        page.payment(metode, pin, ordercodesalah, amount, tipe)
        .set('Authorization', 'Bearer ' + userToken)
        .send(metode, pin, ordercodesalah, amount, tipe)
        .expect(200)
        .end(function(err, res){
            done(err)
            console.log(res.body.data)
        })
    })

    it(testCase.negative.amountsalah, function(done){
        page.payment(metode, pin, ordercode, amountsalah, tipe)
        .set('Authorization', 'Bearer ' + userToken)
        .send(metode, pin, ordercode, amountsalah, tipe)
        .expect(200)
        .end(function(err, res){
            done(err)
            console.log(res.body.data)
        })
    })
})



//Belanja Sukses
describe(testCase.positive.sukses, function(){
    it('Login Sukses', function(done){
        page.login(akun)
        .send(akun)
        .expect(200)
        .end(function(err, res){
            done(err)
            console.log(res.body.data.token)
            userToken = res.body.data.token
            for(var i=0; i<res.body.data.address.length; i++){
                if(res.body.data.address[i].main_address == '1'){
                    console.log(res.body.data.address[i])
                    addressId = res.body.data.address[i].id
                }
            }
        })
    })

    it('Meload data produk di homepage', function(done){
        page.home(login)
        .set('Authorization', 'Bearer ' + userToken)
        .send(login)
        .expect(200)
        .end(function(err, res){
            done(err)
            if(res.body.data[1].see_all == 'product'){
                console.log(res.body.data[1].data[0].product_client_id)
                productClientId = res.body.data[1].data[0].product_client_id
                productClientId1 = res.body.data[1].data[1].product_client_id
            }
        })
    })
    
    it('Menambahkan produk ke cart', function(done){
        if(done){
            page.cartadd()
            .set('Authorization', 'Bearer ' + userToken)
            .send(
                {
                    'items':[
                        {
                            'product_client_id': productClientId,
                            'qty': "1",
                            'note': "Penerima Jennie Kim",
                            'type':"products"
                        }
                    ]
                }
            )
            .expect(200)
            .end(function(err, res){
                done(err)
                console.log(res.body.data)
            })
            } else {
                page.cartadd()
            .set('Authorization', 'Bearer ' + userToken)
            .send(
                {
                    'items':[
                        {
                            'product_client_id': productClientId1,
                            'qty': "1",
                            'note': "Penerima Jennie Kim",
                            'type':"products"
                        }
                    ]
                }
            )
            .expect(200)
            .end(function(err, res){
                done(err)
                console.log(res.body.data)
            })
            }
    })

    it('Data Cart Berhasil', function(done){
        page.cartlist()
        .set('Authorization', 'Bearer ' + userToken)
        .expect(200)
        .end(function(err, res){
            done(err)
            expect(userToken).to.equal(res.body.status.token)
            console.log(res.body.data.cart.code)
            cartCode = res.body.data.cart.code
            for(var i=0; i<res.body.data.cart_details.length; i++){
                console.log(res.body.data.cart_details[i].product_client_id)
                productClientId = res.body.data.cart_details[i].product_client_id
            }
        })
    })

    it('PreCheckout Berhasil', function(done){
        page.preCheckout(cartCode, productClientId)
        .set('Authorization', 'Bearer ' + userToken)
        .send(cartCode, productClientId)
        .expect(200)
        .end(function(err, res){
            done(err)
            console.log(res.body.data)
        })
    })

    it('Checkout Berhasil', function(done){
        page.checkout(addressId, servicejne)
        .set('Authorization', 'Bearer ' + userToken)
        .send(addressId, servicejne)
        .expect(200)
        .end(function(err, res){
            done(err)
            console.log(res.body.data)
            ordercode = res.body.data.order.code
            amount = res.body.data.order.total_amount_final
            //'Hitung Total Amount Final'
            console.log('Total amoun final = total amount - total diskon + delivery fee final = ' + res.body.data.order.total_amount_final)
                expect(res.body.data.order.total_amount_final).to.equal(res.body.data.order.total_amount - res.body.data.order.total_discount + res.body.data.order.delivery_fee_final)

            //'Hitung Delivery Fee Final'
                for(var i=0; i<res.body.data.details.length; i++){
                    console.log(res.body.data.details[i].delivery.fee_final)
                    expect(parseInt(res.body.data.details[i].delivery.discount)).to.equal(parseInt(res.body.data.details[i].diskonongkir_x_qty))
                    expect(parseInt(res.body.data.details[i].delivery.fee_final)).to.equal(parseInt(res.body.data.details[i].sub_delivery));
                    expect(parseInt(res.body.data.order.delivery_fee_final)).to.equal(parseInt(res.body.data.details[i].delivery.fee_final));
                }

            //'Hitung Margin'
                for(i=0; i<res.body.data.details.length; i++){
                    for(k=0; k<res.body.data.details[i].products.length; k++){
                        console.log('total_margin = (price - diskon)/ 1.1 * margin persen = ' + res.body.data.order.total_margin)
                        expect(res.body.data.order.total_margin).to.equal(((res.body.data.details[i].sub_total - res.body.data.details[i].sub_discount)/1.1) * res.body.data.details[i].products[k].total_margin / 100);
                    }
                }

            //'Hitung Actual Cut'
            console.log('total actual cut = total amount final - total margin = ' + res.body.data.order.total_actual_cut)
                expect(res.body.data.order.total_actual_cut).to.equal(res.body.data.order.total_amount_final - res.body.data.order.total_margin);

            //'Hitung produk'
                for(i=0; i<res.body.data.details[0].length; i++){
                    console.log('total_products = ' + res.body.data.order.total_products)
                    expect(res.body.data.order.total_products).to.equal(res.body.data.details[0].products[i].qty);
                }
        })
    })

    it(testCase.positive.sukses, function(done){
        page.payment(metode, pin, ordercode, amount, tipe)
        .set('Authorization', 'Bearer ' + userToken)
        .send(metode, pin, ordercode, amount, tipe)
        .expect(200)
        .end(function(err, res){
            done(err)
            console.log(res.body.data)
        })
    })
})