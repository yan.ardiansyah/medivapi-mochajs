const supertest = require('supertest')
const env = require('dotenv').config()

const listCategoriesAPI = supertest(process.env.DEV_API + '/api/category')

const listCategories = () => listCategoriesAPI.get('')

module.exports = {
    listCategories
}