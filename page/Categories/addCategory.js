const supertest = require('supertest')
const env = require('dotenv').config()

const loginAPI = supertest(process.env.DEV_API + '/api/login')

const login = (email, password, type) => loginAPI.post('')
.query({
    'email' : email, 
    'password' : password, 
    'type' : type
})

const addCategoryAPI = supertest(process.env.DEV_API + '/api/category/add')

const addCategory = (name, file_image) => addCategoryAPI.post('')
.set('Accept', 'application/json')
.query({
    'name': name,
    'file_image': file_image
})

module.exports = {
    login, addCategory
}