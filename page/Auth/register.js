const supertest = require('supertest')
const env = require('dotenv').config()

const regAPI = supertest(process.env.DEV_API + '/api/register')

const register = (name, phone_number, email, password, c_password, registration_from, referral_code, pin, gender, dob, is_tnc) => regAPI.post('')
.query({
    'name': name, 
    'phone_number': phone_number, 
    'email' : email, 
    'password' : password,
    'c_password': c_password,
    'registration_from': registration_from,
    'referral_code': referral_code,
    'pin': pin,
    'gender': gender,
    'dob': dob,
    'is_tnc': is_tnc
})

module.exports = {
    register
}