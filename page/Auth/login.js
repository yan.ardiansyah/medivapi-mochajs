const supertest = require('supertest')
const env = require('dotenv').config()

const loginAPI = supertest(process.env.DEV_API + '/api/login')

const login = (email, password, type) => loginAPI.post('')
.query({'email' : email, 'password' : password, 'type' : type})

module.exports = {
    login
}