const supertest = require('supertest')
const env = require('env').config()

const loginAPI = supertest(process.env.DEV_API + '/api/login')

const logoutAPI = supertest(process.env.DEV_API + '/api/logout')

const login = (email, password, type) => loginAPI.post('')
.query({'email' : email, 'password' : password, 'type' : type})

const logout = () => logoutAPI.post('')
.set('Accept', 'application/json')

module.exports = {
    login, logout
}