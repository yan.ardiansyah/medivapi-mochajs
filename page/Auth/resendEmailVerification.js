const supertest = require('supertest')
const env = require('dotenv').config()

const resendEmailAPI = supertest(process.env.DEV_API + '/api/user/email/resend')

const resendEmail = (email) => resendEmailAPI.post('')
.query({'email': email})

module.exports = {
    resendEmail
}