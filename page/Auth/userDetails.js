const supertest = require('supertest')
const env = require('dotenv').config()

const loginAPI = supertest(process.env.DEV_API + '/api/login')
const userDetailsAPI = supertest(process.env.DEV_API + '/api/details')

const login = (email, password, type) => loginAPI.post('')
.query({'email' : email, 'password' : password, 'type' : type})

const userDetails = (user) => userDetailsAPI.post('')
.set('Accept', 'application/json')

module.exports = {
    login, userDetails
}