const supertest = require('supertest')
const env = require('dotenv').config()

const loginAPI = supertest(process.env.DEV_API + '/api/login')
const login = (email, password, type) => loginAPI.post('')
.query({'email' : email, 'password' : password, 'type' : type})

const homeAPI = supertest(process.env.DEV_API + '/api/homepage')
const home = (login) => homeAPI.post('')
.set('Accept', 'application/json')
.query({'login': login})

const cartAddAPI = supertest(process.env.DEV_API + '/api/cart/add')
const cartadd = (items, product_client_id, qty, note, type) => cartAddAPI.post('')
.set('Accept', 'application/json')
.set('Content-Type', 'application/json')
.query({'items': items, 'product_client_id': product_client_id, 'qty': qty, 'note': note, 'type': type})

const cartListAPI = supertest(process.env.DEV_API + '/api/cart')
const cartlist = () => cartListAPI.get('')
.set('Accept', 'application/json')

const preCheckoutAPI = supertest(process.env.DEV_API + '/api/preCheckout?code=&')
const preCheckout = (code, product_client_id) => preCheckoutAPI.get('')
.set('Accept', 'application/json')
.set('Content-Type', 'application/json')
.query({'code': code, 'product_client_id' : product_client_id})

const checkoutAPI = supertest(process.env.DEV_API + '/api/checkout')
const checkout = (address_id, service_code) => checkoutAPI.post('')
.set('Accept', 'application/json')
.set('Content-Type', 'application/json')
.query({'address_id' : address_id, 'service_code' : service_code})

const paymentAPI = supertest(process.env.DEV_API + '/api/payment')
const payment = (payment_methode, PIN, TRANSIDMERCHANT, AMOUNT, TYPE) => paymentAPI.post('')
.set('Accept', 'application/json')
.query({'payment_methode' : payment_methode, 'PIN' : PIN, 'TRANSIDMERCHANT' : TRANSIDMERCHANT, 'AMOUNT' : AMOUNT, 'TYPE' : TYPE})

module.exports = {
    login, home, cartadd, cartlist, preCheckout, checkout, payment
}